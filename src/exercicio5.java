import java.util.Scanner;

public class exercicio5 {

    public static void main(String[] args) { // método publico estatico principal

        exercicio5(); // metodo
    }

        public static void exercicio5() { // método

        Scanner in = new Scanner(System.in);

        int salario, salarioM, salarioQnt; //variaveis

        System.out.println("Insira o valor do salário: "); // inserir valor de salario
        salario = in.nextInt();

        System.out.println("Insira o salário mínimo: "); // inserir valor de salario minimo
        salarioM = in.nextInt();

        salarioQnt = (salario / salarioM); // salario dividido por salario minimo

            System.out.println("O salário é:  "+salario); // imprimir valor de salario

            System.out.println("O salário minimo é: "+salarioM); // imprimir valor de salario minimo

            System.out.println("A quantidade de salários minimos ganha é: "+salarioQnt); // imprimir  quantidade de salarios minimos
    }
}
